import matrice  # Importez le module "matrice"
import fonctions  # Importez le module "fonctions"


""" Charger et remplir les matrices à partir du fichier CSV """
matrices_par_date = matrice.matrice_par_date('marees_v1.csv')


""" Présentation"""
print("---------- Hello world, welcome to the Ifremer project! ----------")


""" Donne le point le plus haut de la plage qui est immergé """

result = fonctions.point_plus_haut(matrices_par_date)

if result:
    date, x, y = result
    print(f'La marée atteint le point le plus haut à la date du: {date}, aux coordonnées (x={x} et y={y})')
else:
    print("Aucun point le plus haut n'a été trouvé.")


""" Donne les coordonnées des points toujours immergés """

# Utilisez la variable réelle définie précédemment
result_coords = fonctions.valeurs_toujours_egal_1(matrices_par_date)

if result_coords:
    print("Les capteurs sont toujours immergés aux coordonnées:")
    for x, y in result_coords:
        print(f' - (x={x} et y={y})')
else:
    print("Aucun capteur toujours immergé n'a été trouvé.")


""" Donne les coordonnées des points jamais immergés """
# Utilisez la variable réelle définie précédemment
result_coords0 = fonctions.valeurs_toujours_egal_0(matrices_par_date)

if result_coords0:
    print("Les capteurs ne sont jamais immergés aux coordonnées:")
    for x, y in result_coords0:
        print(f' - (x={x} et y={y})')
else:
    print("Aucun capteur jamais immergé n'a été trouvé.")

choix_date_debut = input("Choisissez une date de début entre 2022-01-01-02 et 2022-01-02-12: ")
choix_date_fin = input("Choisissez une date de fin entre 2022-01-01-02 et 2022-01-02-12: ")
choix_x = int(input("Choisissez les coordonnées x entre 0 et 2 d'un point sur la plage: "))
choix_y = int(input("Choisissez les coordonnées y entre 0 et 2 d'un point sur la plage: "))


""" Donne le pourcentage de temps sous l'eau d'un capteur choisi dans un laps de temps choisi. """
result = fonctions.pourcentage_point_sous_leau(matrices_par_date, choix_date_debut, choix_date_fin, choix_x, choix_y)

if result is not None:
    print(f"Le capteur aux coordonnées (x={choix_x}, y={choix_y}) est sous l'eau {result:.2f}% du temps "
          f"entre le {choix_date_debut} et le {choix_date_fin}.")
else:
    print("Aucune date trouvée entre la date de début et la date de fin. ")


""" Donne la date du plus gros coefficient de marée """
fonctions.date_avec_plus_grand_nombre_de_1(matrice.matrices_par_date)


""" Donne la date du plus petit coefficient de marée """
fonctions.date_avec_plus_proche_nombre_de_1_et_0(matrice.matrices_par_date)

