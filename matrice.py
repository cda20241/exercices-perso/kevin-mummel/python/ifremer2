import csv


def matrice_par_date(nom_fichier_csv):
    matrice_de_donnees = lire_fichier_csv(nom_fichier_csv)
    matrices_par_date = remplir_matrices_par_date(matrice_de_donnees)
    return matrices_par_date


def lire_fichier_csv(marees_v1):
    matrice = []
    with open(marees_v1, newline='') as fichier_csv:
        lecteur_csv = csv.reader(fichier_csv, delimiter=';')
        for ligne in lecteur_csv:
            matrice.append(ligne)
    del matrice[0]
    return matrice


nom_fichier_csv = 'marees_v1.csv'
matrice_de_donnees = lire_fichier_csv(nom_fichier_csv)

# Créez un dictionnaire pour stocker les matrices 3x3 avec des dates comme clés
matrices_par_date = {}


def creer_matrice_3x3():
    return [[0] * 3 for _ in range(3)]


# Parcourez la liste de données et remplissez les matrices 3x3 en utilisant la date comme clé
for ligne in matrice_de_donnees:
    # Assurez-vous que la ligne a au moins 4 éléments avant d'extraire la date
    if len(ligne) >= 4:
        date = ligne[3]
        colonne_x = int(ligne[2])  # Inversez les indices x et y ici
        colonne_y = int(ligne[1])  # Inversez les indices x et y ici
        matrice = matrices_par_date.get(date, creer_matrice_3x3())
        matrice[colonne_x][colonne_y] = int(ligne[4])
        matrices_par_date[date] = matrice


# Affichez les matrices 3x3 par date
for date, matrice in matrices_par_date.items():
    print(f'date: {date}')
    for ligne in matrice:
        print(ligne)


def remplir_matrices_par_date(matrice_de_donnees):
    matrices_par_date = {}

    for ligne in matrice_de_donnees:
        if len(ligne) >= 4:
            date = ligne[3]
            colonne_x = int(ligne[2])
            colonne_y = int(ligne[1])
            matrice = matrices_par_date.get(date, creer_matrice_3x3())
            matrice[colonne_x][colonne_y] = int(ligne[4])
            matrices_par_date[date] = matrice

    return matrices_par_date
